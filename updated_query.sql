ALTER TABLE `brands` ADD `status` TINYINT NOT NULL DEFAULT '1' AFTER `validity`;
ALTER TABLE `users` ADD `status` ENUM('0','1') NOT NULL DEFAULT '1' AFTER `gender`;
ALTER TABLE `influencer_language` ADD `id` INT(11) NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`);
ALTER TABLE `influencer_language` ADD `status` ENUM('0','1') NOT NULL DEFAULT '1' AFTER `user_id`, ADD `created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `status`;
ALTER TABLE `influencer_interest` ADD `id` INT(11) NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`);
ALTER TABLE `influencer_interest` ADD `status` ENUM('0','1') NOT NULL DEFAULT '1' AFTER `user_id`, ADD `created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `status`;
ALTER TABLE `influencer_profession` ADD `id` INT(11) NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`);
ALTER TABLE `influencer_profession` ADD `status` ENUM('0','1') NOT NULL DEFAULT '1' AFTER `user_id`, ADD `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `status`;
RENAME TABLE `dev_shopcom`.`influencer_interest` TO `dev_shopcom`.`user_interest`;
RENAME TABLE `dev_shopcom`.`influencer_language` TO `dev_shopcom`.`user_language`;
RENAME TABLE `dev_shopcom`.`influencer_profession` TO `dev_shopcom`.`user_profession`;
ALTER TABLE `influencers` ADD `status` ENUM('0','1') NOT NULL DEFAULT '1' AFTER `number_of_post`;
RENAME TABLE `dev_shopcom`.`content` TO `dev_shopcom`.`contents`;
RENAME TABLE `dev_shopcom`.`performance` TO `dev_shopcom`.`performances`;
RENAME TABLE `dev_shopcom`.`product_image` TO `dev_shopcom`.`product_images`;
RENAME TABLE `dev_shopcom`.`product_interest` TO `dev_shopcom`.`product_interests`;
RENAME TABLE `dev_shopcom`.`product_language` TO `dev_shopcom`.`product_languages`;
RENAME TABLE `dev_shopcom`.`product_profession` TO `dev_shopcom`.`product_professions`;
RENAME TABLE `dev_shopcom`.`product_text` TO `dev_shopcom`.`product_texts`;
RENAME TABLE `dev_shopcom`.`product_video` TO `dev_shopcom`.`product_videos`;
RENAME TABLE `dev_shopcom`.`tracking` TO `dev_shopcom`.`trackings`;
RENAME TABLE `dev_shopcom`.`user_interest` TO `dev_shopcom`.`user_interests`;
RENAME TABLE `dev_shopcom`.`user_language` TO `dev_shopcom`.`user_languages`;
RENAME TABLE `dev_shopcom`.`user_profession` TO `dev_shopcom`.`user_professions`;
ALTER TABLE `products` CHANGE `created_at` `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `products` CHANGE `updated_at` `updated_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `products` CHANGE `mrp` `mrp` INT(11) NULL DEFAULT NULL;
ALTER TABLE `product_images` ADD `status` ENUM('0','1') NOT NULL DEFAULT '1' AFTER `created_by`;
ALTER TABLE `product_interests` ADD `id` INT(11) NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`);
ALTER TABLE `product_interests` ADD `status` ENUM('0','1') NOT NULL DEFAULT '1' AFTER `product_id`, ADD `created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `status`;
ALTER TABLE `product_languages` ADD `id` INT(11) NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`);
ALTER TABLE `product_languages` ADD `status` ENUM('0','1') NOT NULL DEFAULT '1' AFTER `product_id`, ADD `created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `status`;
ALTER TABLE `product_professions` ADD `id` INT(11) NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`);
ALTER TABLE `product_professions` ADD `status` ENUM('0','1') NOT NULL DEFAULT '1' AFTER `product_id`, ADD `created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `status`;
ALTER TABLE `product_texts` ADD `status` ENUM('0','1') NOT NULL DEFAULT '1' AFTER `created_by`;
ALTER TABLE `product_videos` ADD `status` ENUM('0','1') NOT NULL DEFAULT '1' AFTER `created_by`;



