<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::connection('dev')->create('campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('influencer_id');
            $table->bigInteger('product_id');
            $table->integer('campaign_price');
            $table->integer('coupon_id');
            $table->integer('pay_style_id');
            $table->timestamps();
            $table->softDeletes();
            $table->index('influencer_id');
            $table->index('product_id');
            $table->index('coupon_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::connection('dev')->drop('campaigns');
    }
}
