<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::connection('dev')->create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('campaign_id');
            $table->integer('content_id');
            $table->boolean('review_status')->default(false);
            $table->boolean('posted')->default(false);
            $table->timestamps();
            $table->softDeletes();
            $table->index('campaign_id');
            $table->index('content_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::connection('dev')->drop('posts');
    }
}
