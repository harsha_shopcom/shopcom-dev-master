<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSideTables extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::connection('dev')->create('influencer_language', function (Blueprint $table) {
            $table->string('language', 25);
            $table->bigInteger('user_id');
            $table->index('language');
            $table->index('user_id');
        });
        Schema::connection('dev')->create('product_language', function (Blueprint $table) {
            $table->string('language', 25);
            $table->bigInteger('product_id');
            $table->index('language');
            $table->index('product_id');
        });
        Schema::connection('dev')->create('influencer_interest', function (Blueprint $table) {
            $table->string('interest', 25);
            $table->bigInteger('user_id');
            $table->index('interest');
            $table->index('user_id');
        });
        Schema::connection('dev')->create('product_interest', function (Blueprint $table) {
            $table->string('interest', 25);
            $table->bigInteger('product_id');
            $table->index('interest');
            $table->index('product_id');
        });
        Schema::connection('dev')->create('influencer_profession', function (Blueprint $table) {
            $table->string('profession', 25);
            $table->bigInteger('user_id');
            $table->index('profession');
            $table->index('user_id');
        });
        Schema::connection('dev')->create('product_profession', function (Blueprint $table) {
            $table->string('profession', 25);
            $table->bigInteger('product_id');
            $table->index('profession');
            $table->index('product_id');
        });
        Schema::connection('dev')->create('product_text', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('product_id');
            $table->text('content');
            $table->string('type', 30)->nullable();
            $table->string('language', 30)->default('english');
            $table->integer('characters')->nullable();
            $table->string('created_by', 30)->nullable();
            $table->dateTime('created_at')->useCurrent();
            $table->softDeletes();
            $table->index('product_id');
        });
        Schema::connection('dev')->create('product_image', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('product_id');
            $table->text('content_key');
            $table->string('type', 30)->nullable();
            $table->string('language', 30)->default('english');
            $table->integer('size')->nullable();
            $table->string('created_by', 30)->nullable();
            $table->dateTime('created_at')->useCurrent();
            $table->softDeletes();
            $table->index('product_id');
        });
        Schema::connection('dev')->create('product_video', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('product_id');
            $table->text('content');
            $table->string('type', 30)->nullable();
            $table->string('language', 30)->default('english');
            $table->integer('length')->nullable();
            $table->string('created_by', 30)->nullable();
            $table->dateTime('created_at')->useCurrent();
            $table->softDeletes();
            $table->index('product_id');
        });
        Schema::connection('dev')->create('coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('product_id');
            $table->bigInteger('user_id');
            $table->string('name', 25);
            $table->string('description', 100);
            $table->string('type', 30)->nullable();
            $table->integer('limit')->nullable();
            $table->integer('used')->default(0);
            $table->dateTime('created_at')->useCurrent();
            $table->dateTime('updated_at')->nullable();
            $table->dateTime('expiry_date')->nullable();
            $table->softDeletes();
            $table->index('product_id');
            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::connection('dev')->drop('influencer_language');
        Schema::connection('dev')->drop('product_language');
        Schema::connection('dev')->drop('influencer_interest');
        Schema::connection('dev')->drop('product_interest');
        Schema::connection('dev')->drop('influencer_profession');
        Schema::connection('dev')->drop('product_profession');
        Schema::connection('dev')->drop('product_text');
        Schema::connection('dev')->drop('product_image');
        Schema::connection('dev')->drop('product_video');
        Schema::connection('dev')->drop('coupons');
    }
}
