<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrandsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::connection('dev')->create('brands', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('value', 50);
            $table->string('website', 50);
            $table->string('type_of_contract', 100);
            $table->bigInteger('contact');
            $table->date('validity');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::connection('dev')->drop('brands');
    }
}
