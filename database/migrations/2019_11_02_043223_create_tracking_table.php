<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackingTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::connection('dev')->create('tracking', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post_id');
            $table->enum('track_type', ['page_level', 'event', 'error']);
            $table->string('ip', 50)->default(null);
            $table->string('referrer', 100)->default(null);
            $table->string('location', 100)->default(null);
            $table->string('user_agent', 100)->default(null);
            $table->string('language', 100)->default(null);
            $table->dateTime('datetime');
            $table->index('post_id');
            $table->index('ip');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::connection('dev')->drop('tracking');
    }
}
