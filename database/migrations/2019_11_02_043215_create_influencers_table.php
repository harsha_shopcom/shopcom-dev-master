<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfluencersTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::connection('dev')->create('influencers', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id');
            $table->string('social_id', 50);
            $table->string('platform', 50);
            $table->integer('followers')->nullable();
            $table->integer('engagement')->nullable();
            $table->integer('number_of_post')->default(0);
            $table->timestamps();
            $table->softDeletes();
            $table->index('user_id');
            $table->index('platform');
            $table->index('followers');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::connection('dev')->drop('influencers');
    }
}
