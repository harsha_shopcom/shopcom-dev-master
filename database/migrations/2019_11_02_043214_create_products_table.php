<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::connection('dev')->create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('brand_id');
            $table->string('name', 100);
            $table->string('redirect_url', 100);
            $table->integer('mrp');
            $table->softDeletes();
            $table->timestamps();
            $table->index('brand_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::connection('dev')->drop('products');
    }
}
