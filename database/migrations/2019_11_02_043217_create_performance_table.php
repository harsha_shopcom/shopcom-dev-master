<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerformanceTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::connection('dev')->create('performance', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post_id');
            $table->integer('leads')->default(0);
            $table->integer('unique_leads')->default(0);
            $table->integer('likes')->default(0);
            $table->integer('comments')->default(0);
            $table->integer('shares')->default(0);
            $table->integer('views')->default(0);
            $table->timestamps();
            $table->index('post_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::connection('dev')->drop('performance');
    }
}
