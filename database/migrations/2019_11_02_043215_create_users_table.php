<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::connection('dev')->create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->bigInteger('contact');
            $table->string('email', 50)->nullable();
            $table->string('location', 50)->nullable();
            $table->integer('age')->nullable();
            $table->string('device', 50)->nullable();
            $table->enum('gender', ['male', 'female', 'default']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::connection('dev')->drop('users');
    }
}
