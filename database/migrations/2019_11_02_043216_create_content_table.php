<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::connection('dev')->create('content', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hash', 100);
            $table->string('text_ids', 100);
            $table->string('image_ids', 100);
            $table->string('video_ids', 100);
            $table->index('hash');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::connection('dev')->drop('content');
    }
}
