<?php

namespace App\Services;

use DB;

class DataFetch
{
    public function __construct()
    {
        $this->connection = env('DATABASE_CONNECTION', 'dev');
    }

    public function influencerFeed()
    {
        $data = DB::connection($this->connection)->table('products')->get();
    }

    public function influencerStore($userId)
    {
        $data = DB::connection($this->connection)->table('campaigns')->get();
    }
}
