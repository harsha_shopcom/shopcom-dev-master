<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use DB;
use Validator;
use App\Models\Brands;
use App\Models\Users;
use App\Models\Products;
use App\Models\Influencers;

class RegistrationController extends Controller
{
    // ToDo add validation checks

    public function registerBrand(Request $request)
    {
        $name = $request->input('name');
        $value = $request->has('value') ? $request->input('value') : '';
        $website = $request->has('website') ? $request->input('website') : '';
        $typeOfContract = $request->has('type_of_contract') ? $request->input('type_of_contract') : '';
        $contact = $request->has('contact') ? $request->input('contact') : '';
        $validity = $request->input('validity');
        if (!$name || !$validity) {
            return response()->json(['success' => false, 'message' => 'Brand Name & Validity Mandatory.'], 422);
        }
        $brand = new Brands();
        $brand->name = $name;
        $brand->value = $value;
        $brand->website = $website;
        $brand->type_of_contract = $typeOfContract;
        $brand->contact = $contact;
        $brand->validity = $validity;
        $result = $brand->save();
        if ($result) {
            return response()->json(['success' => true, 'message' => 'Brand Registered Successfully.', 'brand_id' => $brand->id], 200);
        }

        return response()->json(['success' => false, 'message' => 'Something Went Wrong, Please Try Again.'], 422);
    }

    public function registerUser(Request $request)
    {
        // laravel validator facade for controlled validations
        $validator = Validator::make($request->all(), [
            'contact' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            // get messages -> $validator->messages());

            return response()->json(['success' => false, 'message' => 'Please provide the contact number.'], 422);
        }
        $contact = $request->input('contact');
        $user = Users::where('contact', $contact)->first();
        if (!empty($user)) {
            return response()->json(['success' => false, 'message' => 'This contact number is already registered with us. Please try another.']);
        }

        $name = $request->has('name') ? $request->input('name') : '';
        $email = $request->has('email') ? $request->input('email') : '';
        $location = $request->has('location') ? $request->input('location') : '';
        $age = $request->has('age') ? $request->input('age') : -1;
        $gender = $request->has('gender') ? $request->input('gender') : 'default';
        $device = $request->has('device') ? $request->input('device') : 'not defined';

        $success = DB::table('users')->insert([
            'name' => $name,
            'contact' => $contact,
            'email' => $email,
            'location' => $location,
            'age' => $age,
            'gender' => $gender,
            'device' => $device,
        ]);

        return response()->json(['success' => true, 'message' => 'User Registered Successfully', 'user_id' => DB::getPdo()->lastInsertId()]);
    }

    public function registerInfluencer(Request $request)
    {
        //lumen helper $this->validate, directly returns {"platform":["The platform field is required."]} on failure
        $this->validate($request, [
            'user_id' => 'required|numeric',
            'social_id' => 'required',
            'platform' => 'required|string',
        ]);

        $userId = $request->input('user_id');
        $socialId = $request->input('social_id');
        $platform = $request->input('platform');

        // check if influencer social id already exist.
        $influencer = Influencers::where([
            ['platform', $platform],
            ['social_id', $socialId],
        ])->first();
        if (!empty($influencer)) {
            if ($influencer->user_id !== $userId) {
                return response()->json(['success' => false, 'message' => 'This social id is already registered with us. Please try another.'], 403);
            }

            return response()->json(['success' => false, 'message' => 'You are already registered with us.']);
        }

        $followers = $request->has('followers') ? $request->input('followers') : '';
        $engagement = $request->has('engagement') ? $request->input('engagement') : '';
        $number_of_post = $request->has('number_of_post') ? $request->input('number_of_post') : '';

        // save social influencer id of the user.
        $influencer = new Influencers();
        $influencer->user_id = $userId;
        $influencer->social_id = $socialId;
        $influencer->platform = $platform;
        $influencer->followers = $followers;
        $influencer->engagement = $engagement;
        $influencer->number_of_post = $number_of_post;
        $result = $influencer->save();
        if ($result) {
            $influencer_id = $influencer->id;
            
            // save user language if defined
            if ($request->has('languages')) {
                $languages = $request->input('languages');
                $this->insertUserBehaviours('user_languages', 'language', $languages, $userId);
            }
	
            // save user interest if defined
            if ($request->has('interest')) {
                $interests = $request->input('interest');
				$this->insertUserBehaviours('user_interests', 'interest', $interests, $userId);
            }
            // save user profession if defined
            if ($request->has('profession')) {
                $professions = $request->input('profession');
				$this->insertUserBehaviours('user_professions', 'profession', $professions, $userId);
            }

            return response()->json(['success' => true, 'message' => 'Influencers Registered Successfully.', 'influencer_id' => $influencer_id]);
        }

        return response()->json(['success' => false, 'message' => 'Something Went Wrong, Please Try Again.']);
    }

    public function registerProduct(Request $request)
    {
        $brandId = $request->input('brand_id');
        $name = $request->input('name');
        $redirectUrl = $request->has('redirect_url') ? $request->input('redirect_url') : '';
        $mrp = $request->has('mrp') ? $request->input('mrp') : null;

        
		$this->validate($request, [
            'brand_id' => 'required|numeric',
            'name' => 'required'
        ]);

        $product = new Products();
        $product->name = $name;
        $product->brand_id = $brandId;
        $product->redirect_url = $redirectUrl;
        $product->mrp = $mrp;

        $result = $product->save();
        if ($result) {
            $productId = $product->id;

            // save product language if defined
            if ($request->has('languages')) {
                $languages = $request->input('languages');
				$this->insertUserBehaviours('product_languages', 'language', $languages, $productId);
            }
            // save product interest if defined
            if ($request->has('interest')) {
                $interests = $request->input('interest');
				$this->insertUserBehaviours('product_interests', 'interest', $interests, $productId);
            }
            // save product profession if defined
            if ($request->has('profession')) {
                $professions = $request->input('profession');
				$this->insertUserBehaviours('product_professions', 'profession', $professions, $productId);
            }
			// save product texts if defined
			$t[] = ['content' => 'text1 is entered'];
			$t[] = ['content' => 'text2 is entered'];
			$t[] = ['content' => 'text3 is entered', 'type' =>'beauty'];
            if($t){
            $texts = $t;
            //if ($request->has('profession')) {
                //$professions = $request->input('profession');
				$this->insertProductText($texts, $productId);
            }

            return response()->json(['success' => true, 'message' => 'Product Saved Successfully.', 'product_id' => $product->id]);
        }

        return response()->json(['success' => false, 'message' => 'Something Went Wrong, Please Try Again.']);
    }
	
	public function insertUserBehaviours($tableName, $fieldName, $skills, $userId)
    {
        $insertArray = [];
        foreach ($skills as $skill) {
            $insertArray[] = ['user_id' => $userId, $fieldName => $skill];
        }

        return DB::table($tableName)->insert($insertArray);
    }
	
	public function insertProductBehaviours($tableName, $fieldName, $attributes, $productId)
    {
        $insertArray = [];
        foreach ($attributes as $attribute) {
            $insertArray[] = ['product_id' => $productId, $fieldName => $attribute];
        }

        return DB::table($tableName)->insert($insertArray);
    }

    public function insertProductText($texts, $productId)
    {
		$insertArray = [];
        foreach ($texts as $text) {
			$insertArray[] = [
				'content' => isset($text['content']) ? $text['content']: "",
				'type' => isset($text['type']) ? $text['type']: NULL,
				'language' => isset($text['language']) ? $text['language']: 'english',
				'characters' => isset($text['characters']) ? $text['characters']: NULL,
				'created_by' => isset($text['created_by']) ? $text['created_by']: NULL,
				'product_id' => $productId,
			];
        }

        return DB::table('product_texts')->insert($insertArray);
    }
}
//$t = ['hindi111','english111'];
//if($t){
//$languages = $t;
?>