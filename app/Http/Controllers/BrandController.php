<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Brands;

class BrandController extends Controller
{
    /* to get the all brand details
    http://localhost/shopcom-dev-master/public/all-brand
    */
    public function index(Request $request)
    {
        $brands = Brands::all(['name', 'value', 'website']);

        return response()->json($brands, 200);
    }

    /* to get the brand details by brand id
    http://localhost/shopcom-dev-master/public/brand-by-id/1111111
    */
    public function getBrandById(Request $request, $id)
    {
        $brand = Brands::find($id, ['name', 'value', 'website']);

        return response()->json($brand, 200);
    }
}
?>