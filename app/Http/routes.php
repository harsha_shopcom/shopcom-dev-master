<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

// ToDo: have to create middleware
// url to access these routes : dev.shopcom.in/dev/tarun/shopcom/public/{route_name} for
// eg: http://dev.shopcom.in/tarun/shopcom/public/register-brand
$app->group(['namespace' => 'App\Http\Controllers'], function () use ($app) {
    $app->post('/register-brand', 'RegistrationController@registerBrand');
    $app->post('/register-product', 'RegistrationController@registerProduct');
    $app->post('/register-user', 'RegistrationController@registerUser');
    $app->post('/register-influencer', 'RegistrationController@registerInfluencer');
});

$app->group(['namespace' => 'App\Http\Controllers'], function () use ($app) {
    $app->get('/all-brand', 'BrandController@index');
    $app->get('/brand-by-id/{brand_id}', 'BrandController@getBrandById');
});